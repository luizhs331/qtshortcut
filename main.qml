import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.10

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Button{
        id:novoButton
        width:  200
        height: 50
        anchors.centerIn: parent
        Label {
            text: "Abrir Popup"
            font.family: "Helvetica"
            font.pointSize: 15
            color: "black"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            opacity: 0.6
          }
        background: Rectangle{
        radius: 10
        visible: true
        color:"#fff"
        border.color: mouseArea.containsMouse ? "#11cdef" : "#dfe0e1"
        }
    MouseArea{
        id:mouseArea
        anchors.fill: parent
        hoverEnabled: true
            onClicked: {
               popupInicial.open()
           }
        }
    }

    Shortcut {
        sequence: "Ctrl+E"
        onActivated: popupInicial.open()
    }

    Popup{
    id:popupInicial
    width: parent.width / 3

    Shortcut {
        sequence: "Ctrl+S"
        onActivated: name.text = textField.text
    }

    Column{
    anchors.centerIn: parent
    spacing: 20

    Label {
        id:name
        text: "Abrir Popup"
        font.family: "Helvetica"
        font.pointSize: 15
        color: "black"
        opacity: 0.6
        wrapMode: "WordWrap"
    }

    TextField{
    id:textField
    width: 300
    height: 50
    }
}
    height: 700
    modal: true
    focus: true
    anchors.centerIn: parent
    background: Rectangle{
    border.color: "#fff"
    radius: 4
    }
    }
}
